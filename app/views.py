from django.shortcuts import render, redirect
from datetime import datetime, timezone
from django.db.models import Max
from .forms import LoginForm, FormOfRegistration, SportifForm, AssistForm, changePassword, changeMail, changeNum, ContactForm, pwdForget
from .models import Evenement, User, Assist
from django.core.mail import send_mail

# Functions
def checkSession(request):
    if 'logged_user_id' in request.session:
        logged_user_id = request.session['logged_user_id']
        user_logged = User.objects.get(pseudo=logged_user_id)
        return user_logged
    else:
        return 'no'

def button(request):
    del request.session['logged_user_id']
    return redirect('/app/login')

def thanks(request):
    return render(request, 'thanks.html');

# Contact
def sendContact(request):
    if checkSession(request) != 'no':
        if len(request.POST) > 0:  # check if form sends
            form = ContactForm(request.POST)
            if form.is_valid():
                """
                name = form.cleaned_data['name']
                mail = form.cleaned_data['mail']
                subject = form.cleaned_data['subject']
                message = form.cleaned_data['message']

                recipients = ['aurelien2107@orange.fr']
                recipients.append(mail)

                send_mail(subject, message, mail, recipients)
                """
                return redirect('/app/thanks')
        else:
            form = ContactForm()
            return render(request, 'contact.html', {'form': form, 'user_logged': checkSession(request)})
    else:
        form = ContactForm()
        return render(request, 'contact.html', {'form': form})


# Header/Footer
def header(request):
    return render(request, 'header.html')

def footer(request):
    return render(request, 'footer.html')

# Interface for the user#
def member(request):
    if 'logged_user_id' in request.session:
        logged_user_id = request.session['logged_user_id']
        user_logged = User.objects.get(pseudo=logged_user_id)
        return render(request, 'member.html', {'user_logged': user_logged})
    else:
        # return redirect('/app/login')
        return render(request, 'accueil.html')

def addPanier(request, var):
    if checkSession(request) != 'no':
        if 'logged_user_id' in request.session:
            request.session['panier'] = var
            return redirect('/app/panier')
    else:
        return redirect('/app/login')

def delPanier(request):
    request.session['panier'] = 'no'
    return redirect('/app/panier')

def panier(request):
    if checkSession(request) != 'no':
        return render(request, 'panier.html')
    else:
        return redirect('/app/login')


def pay(request):
    if checkSession(request) != 'no':
        return render(request, 'payer.html')
    else:
        return redirect('/app/accueil')


# many forms in one page
def mailchanged(request):
    if checkSession(request) != 'no':
        """if len(request.POST) > 0:
            form1 = changeMail(request.POST)
            if form1.is_valid():
                newMail = request.POST['newMail']
                email = request.POST['email']
                for u in User.objects.all():
                    if u.email == email:
                        u.email = newMail
                        u.save()
                        return redirect('/app/member')
                return redirect('/app/parametres')
            else:
                return render(request, 'parametres.html', {'form1': form1})
        elif len(request.POST) > 0:
            form2 = changeNum(request.POST)
            if form2.is_valid():
                newNum = request.POST['newNum']
                num = request.POST['num']
                for u in User.objects.all():
                    if u.phone_number == num:
                        u.phone_number = newNum
                        u.save()
                        return redirect('/app/member')
                return redirect('/app/parametres')
            else:
                return render(request, 'parametres.html', {'form2': form2})"""
        if len(request.POST) > 0:
            form = changePassword(request.POST)
            if form.is_valid():
                email = request.POST['email']
                newpass = request.POST['newpass']
                for u in User.objects.all():
                    if u.email == email:
                        u.password = newpass
                        u.save()
                        return redirect('/app/member')
                return redirect('/app/parametres')
            else:
                return render(request, 'parametres.html', {'form': form})
        else:
            # form1 = changeMail()
            form = changePassword()
            # form2 = changeNum()
            # return render(request, 'parametres.html', {'form1': form1, 'form': form, 'form2': form2})
            return render(request, 'parametres.html', {'form': form})
    else:
        return redirect('/app/accueil')


def assist(request):
    if checkSession(request) != 'no':
        if len(request.POST) > 0:
            form = AssistForm(request.POST)
            if form.is_valid():
                sujet = request.POST['sujet']

                sujet = Assist(
                    sujet=request.POST['sujet'],
                    message=request.POST['message'],
                    envoyeur=request.POST['envoyeur']
                )
                sujet.save()
            else:
                print(form.errors)
                return render(request, 'assistance.html', {'form': form})
        else:
            form = AssistForm()
            return render(request, 'assistance.html', {'form': form})
    else:
        return redirect('/app/accueil')


# Functions to save forms (connexion)
def register(request):
    if len(request.POST) > 0:  # check if form sends
        form = FormOfRegistration(request.POST)
        student = request.POST['idEtudiant']
        student = User(
            pseudo=request.POST['username'],
            idEtudiant=request.POST['idEtudiant'],
            email=request.POST['email'],
            phone_number=request.POST['cellular'],
            password=request.POST['psw'],
        )
        student.save()
        if form.is_valid():
            return redirect('/app/login')

        else:
            return render(request, 'inscription.html', {'form': form})
    else:
        form = FormOfRegistration()
        return render(request, 'inscription.html', {'form': form})


def login(request):
    if len(request.POST) > 0:  # check if form sends
        form = LoginForm(request.POST)
        if form.is_valid():
            request.session['panier'] = 'no'
            user_pseudo = form.cleaned_data['username']
            user_logged = User.objects.get(pseudo=user_pseudo)
            request.session['logged_user_id'] = user_logged.pseudo
            return redirect('/app/member')
        else:
            print(form.errors)
            return render(request, 'connexion.html', {'form': form})
    else:
        form = LoginForm()
        return render(request, 'connexion.html', {'form': form})


def mdpForget(request):
    if len(request.POST) > 0:
        email = request.POST['email']
        for u in User.objects.all():
            if u.email == email:
                # TODO send email with new mdp to change.
                return redirect('/app/sendNewMdp')
        return redirect('/app/register')
    else:
        return redirect('/app/register')


def sendNewMdp(request):
    # TODO gérer envoie new mdp par mail
    return redirect('/app/thanks')

def accueil(request):
    if checkSession(request) != 'no':
        return render(request, 'accueil.html', {'user_logged': checkSession(request)})
    else:
        return render(request, 'accueil.html')

def sportif(request):
    newdate = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    for e in Evenement.objects.all():
        if str(e.dateDebutEvenement) < newdate:
            e.delete()
    Event = Evenement.objects.filter(typeEvenement='s')
    nbEvent = Event.count()
    if(nbEvent > 0):
        maxSportif = Event.annotate(maxidSportif=Max('id'))[nbEvent - 1].maxidSportif
    else:
        maxSportif = 0
    if checkSession(request) != 'no':
        return render(request, 'sportif.html', {'user_logged': checkSession(request), 'Event': Event, 'maxSportif': maxSportif, 'nbEvent': nbEvent})
    else:
        return render(request, 'sportif.html', {'Event': Event, 'maxSportif': maxSportif, 'nbEvent': nbEvent})

def culturel(request):
    newdate = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    for e in Evenement.objects.all():
        if str(e.dateDebutEvenement) < newdate:
            e.delete()
    Event = Evenement.objects.filter(typeEvenement='c')
    nbEvent = Event.count()
    if (nbEvent > 0):
        maxCulturel = Event.annotate(maxidCulturel=Max('id'))[nbEvent - 1].maxidCulturel
    else:
        maxCulturel = 0
    if checkSession(request) != 'no':
        return render(request, 'culturel.html', {'user_logged': checkSession(request), 'Event': Event, 'maxCulturel': maxCulturel, 'nbEvent': nbEvent})
    else:
        return render(request, 'culturel.html', {'Event': Event, 'maxCulturel': maxCulturel, 'nbEvent': nbEvent})

def Reservation(request):
    Event = Evenement.objects.all()
    return render(request, "reservation.html", {'user_logged': checkSession(request), 'Event' : Event})