from django.contrib import admin
from .models import User, Evenement
from datetime import datetime

admin.site.register(User)


def make_published(SportifAdmin, request, queryset):
    queryset.update(status='published')
    make_published.short_description = "Marquer les évènements qui ont été publiés"


class EvenementAdmin(admin.ModelAdmin):
    list_display = ('titre', 'legende', 'image',
                    'dateDebutEvenement', 'typeEvenement', 'nombreDePlaces', 'prixEvenement')  # champs pour différencier les données
    fieldsets = [  # champs afficher pour le formulaire
        (None, {'fields': ['titre', 'legende', 'image',  'dateDebutEvenement', 'typeEvenement', 'nombreDePlaces', 'prixEvenement']})
    ]
    actions = [make_published]


admin.site.register(Evenement, EvenementAdmin)

