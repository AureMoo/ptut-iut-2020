from django.db import models

STATUS_CHOICES = [
    ('d', 'Draft'),
    ('p', 'Published'),
    ('w', 'Withdrawn'),
]

TYPE_EVENT = {
    ('s', 'Sportif'),
    ('c', 'Culturel'),
}

class Assist(models.Model):
    sujet = models.CharField(max_length=100)
    message = models.CharField(max_length=500)
    envoyeur = models.EmailField(max_length=20)

    def __str__(self):
        return self.sujet + " "+ self.message+ " "+ self.envoyeur


class User(models.Model):
    pseudo = models.CharField(max_length=100)
    idEtudiant = models.CharField(max_length=8)  # pXXXXXXX
    email = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=20)
    #event = models.CharField(max_length=1000, default="NULL")
    password = models.CharField(max_length=1000, default="MDP")

    # TODO passwords hashing

    def __str__(self):  # how it is writting on graphic db "/admin"
        return self.pseudo + " " + self.idEtudiant

class Evenement(models.Model):
    titre = models.CharField(max_length=100)
    legende = models.CharField(max_length=100)
    image = models.ImageField(upload_to="")
    dateDebutEvenement = models.DateTimeField(max_length=100)
    typeEvenement = models.CharField(choices=TYPE_EVENT, default='sportif', max_length=100)
    prixEvenement = models.IntegerField(default=1)
    nombreDePlaces = models.IntegerField(default=0)

    def __str__(self):
        return self.titre + " " + self.legende + " " + self.typeEvenement
