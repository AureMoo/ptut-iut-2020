from . import views
from django.urls import path

urlpatterns = [
    path('sportif', views.sportif),
    path('panier', views.panier),
    path('delPanier', views.delPanier),
    path('member', views.member),
    path('login', views.login),
    path('button', views.button),
    path('contact', views.sendContact),
    path('register', views.register, name='register'),
    path('', views.accueil),
    path('mdpForget', views.mdpForget, name='mdpForget'),
    path('accueil', views.accueil),
    path('mdpForget', views.mdpForget),
    path('thanks', views.thanks),
    path('sendNewMdp', views.sendNewMdp),
    path('header', views.header),
    path('addPanier/<var>', views.addPanier),
    path('reservation', views.Reservation),
    path('culturel', views.culturel),
    path('footer', views.footer),
    path('button', views.button),
    path('assist', views.assist),
    path('pay', views.pay),
    path('parametres', views.mailchanged),
]
