from django import forms
from django.db import models
from .models import Evenement, User, Assist

class changeNum(forms.Form):
    num = forms.CharField(label="Votre numéro")
    newNum = forms.CharField(label="Votre nouveau numéro")

    class Meta:
        model = User
        fields = ['num', 'newNum']
        def clean(self):
            cleaned_data = super(changeNum, self).clean()
            num = cleaned_data.get("num")
            newNum = cleaned_data.get("newNum")

            return cleaned_data

class changeMail(forms.Form):
    email = forms.EmailField(label="Votre adresse e-mail")
    newMail = forms.EmailField(label="Votre nouvelle adresse e-mail")

    class Meta:
        model = User
        fields = ['email', 'newMail']
        def clean(self):
            cleaned_data = super(changeMail, self).clean()
            email = cleaned_data.get("email")
            newMail = cleaned_data.get("newMail")

            return cleaned_data

class changePassword(forms.Form):
    email = forms.EmailField(label="Votre adresse e-mail")
    newpass = forms.CharField(label='Nouveau mot de passe')
    confirmnewpass = forms.CharField(label="Confirmez votre nouveau mot de passe")

    class Meta:
        model = User
        fields = ['email', 'newpass', 'confirmnewpass']
        def clean(self):
            cleaned_data = super(changePassword, self).clean()
            email = cleaned_data.get("email")
            newpass = cleaned_data.get("newpass")
            confirmnewpass = cleaned_data.get("confirmnewpass")

            if newpass != confirmnewpass:  # check if both passwords are same
                raise forms.ValidationError("Les mots de passe sont différents.")

            return cleaned_data

class AssistForm(forms.Form):
    sujet = forms.CharField(label='sujet', max_length=100)
    message = forms.CharField(label='Votre question', widget=forms.Textarea)
    envoyeur = forms.EmailField(label="Votre adresse e-mail")

    class Meta:
        model = Assist
        fields = ['sujet', 'message', 'envoyeur']
        def clean(self):
            cleaned_data=super(AssistForm, self).clean()
            sujet = cleaned_data.get("sujet")
            message = cleaned_data.get("message")
            envoyeur = cleaned_data.get("envoyeur")

            return cleaned_data

class FormOfRegistration(forms.Form):
    username = forms.CharField(label='Nom d\'utilisateur', max_length=100)
    email = forms.EmailField(label='eMail', max_length=200)
    idEtudiant = forms.CharField(label='p1XXXXXX', max_length=8)
    cellular = forms.CharField(label="Numéro de téléphone")
    psw = forms.CharField(label='Mot de passe', widget=forms.PasswordInput)
    checkPsw = forms.CharField(label='Mot de passe', widget=forms.PasswordInput)

    class Meta:
        model = User

    def clean(self):
        cleaned_data = super(FormOfRegistration, self).clean()
        psw = cleaned_data.get("psw")
        checkPsw = cleaned_data.get("checkPsw")

        if psw != checkPsw:  # check if both passwords are same
                raise forms.ValidationError("Les mots de passe sont différents.")
        return cleaned_data


class LoginForm(forms.Form):
    username = forms.CharField(label='Nom d\'utilisateur')
    password = forms.CharField(label='Mot de passe', widget=forms.PasswordInput)

    def clean(self):
        cleaned_data = super(LoginForm, self).clean()
        username = cleaned_data.get("username")
        password = cleaned_data.get("password")

        if username and password:  # check if both elements are valid
            result = User.objects.filter(password=password, pseudo=username)
            if len(result) != 1:  # Do the same with password but both using database
                raise forms.ValidationError("Pseudo ou mot de passe erroné.")
        return cleaned_data

class ContactForm(forms.Form):
    name = forms.CharField(label="Votre nom", max_length=200)
    mail = forms.EmailField(label="Adresse mail", max_length=200)
    subject = forms.CharField(label="Objet", max_length=200)
    message = forms.CharField(label="Votre message")

    def clean(self):
        cleaned_data = super(ContactForm, self).clean()
        mail = cleaned_data.get("mail")
        subject = cleaned_data.get("subject")
        message = cleaned_data.get("message")

        """
        if mail and subject and message:  # check if both elements are valid
            print("ok form contact");
        else:
            raise forms.ValidationError("Champs manquant.")
        """
        return cleaned_data

class pwdForget(forms.Form):
    email = forms.EmailField(label="Votre email")

    def clean(self):
        cleaned_data = super(ContactForm, self).clean()
        email = cleaned_data.get("email")
        return cleaned_data

STATUS_CHOICES = [
    ('d', 'Draft'),
    ('p', 'Published'),
    ('w', 'Withdrawn'),
]

class SportifForm(forms.Form):
    titre = forms.CharField(label="Titre de votre évènement", max_length=100, required=True)
    legende = forms.CharField(label="Legende de votre évènement", max_length=100, required=True)
    image = forms.ImageField(label="Votre image", required=True)
    DateDebutEvenement = forms.DateTimeField(label="Date de début de votre évènement", required=True)
    typeEvenement = forms.ChoiceField()
    NombreDePlaces = forms.IntegerField(required=True)
    prixEvenement = forms.IntegerField(required=True)

    class Meta:
        model = Evenement
        fields = ['titre', 'legende', 'image', 'DateDebutEvenement', 'typeEvenement', 'prixEvenement']

        def clean(self):
            cleaned_data = super(SportifForm, self).clean()
            return cleaned_data
