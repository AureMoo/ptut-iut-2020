# Generated by Django 4.0.2 on 2022-02-16 10:20

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Assist',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sujet', models.CharField(max_length=100)),
                ('message', models.CharField(max_length=500)),
                ('envoyeur', models.EmailField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Evenement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titre', models.CharField(max_length=100)),
                ('legende', models.CharField(max_length=100)),
                ('image', models.ImageField(upload_to='')),
                ('dateDebutEvenement', models.DateTimeField(max_length=100)),
                ('typeEvenement', models.CharField(choices=[('s', 'Sportif'), ('c', 'Culturel')], default='sportif', max_length=100)),
                ('prixEvenement', models.IntegerField(default=1)),
                ('nombreDePlaces', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pseudo', models.CharField(max_length=100)),
                ('idEtudiant', models.CharField(max_length=8)),
                ('email', models.CharField(max_length=200)),
                ('phone_number', models.CharField(max_length=20)),
                ('password', models.CharField(default='MDP', max_length=1000)),
            ],
        ),
    ]
