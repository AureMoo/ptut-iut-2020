# PTut 2è Année IUT Informatique

Projet de deuxième année d'IUT sur 1 an.
*IUT Lyon 1 - Bourg-en-Bresse - 2019/2020*

## Team

Aurélien Moote : Chef de projet et développeur front/backend
Maxime G. : Développeur front/backend
Hugo C. : Développeur frontend
Charly B. : Développeur frontend

## Languages

python : 3.9.0 <br>
pip : 20.2.3 <br>
EB CLI 3.20.3 (Python 3.9.0) <br>
django : '4.0.2' <br>

## Installation

A la racine du projet:
```
python3 manage.py runserver
```

Aller à <br>
http://127.0.0.1:8000/app

*Attention, il n'y pas de données enregistrées dans la base de donnée, d'où le fait que le site soit vide. Il est possible de nourrir la base de données en passant par l'interface Administrateur*



